package application;
//David Hang, 2234338
import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[]args) {
        Bicycle[] bikes = new Bicycle[4];

        bikes[0] = new Bicycle("Specialized", 23, 60);
        bikes[1] = new Bicycle("Non-Specialized", 12, 40);
        bikes[2] = new Bicycle("Ordinary", 6, 30);
        bikes[3] = new Bicycle("Questionable", 2, 10);

        for(int i = 0; i < bikes.length; i++) {
            System.out.println(bikes[i]);
        }
    }
}
